// make with package 

import java.io.IOException; 

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.compress.SnappyCodec;

public class HW1 {


  // public class Mapper<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
  public static class CustomMapper
       extends Mapper<Object, Text, Text, IntWritable>{

    private final static IntWritable one = new IntWritable(1);    
    private Text browser = new Text();                            

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      
      // if access.log string(Text value) contains browser name then set (map,key) to (browser,1)
      // order of search is significant, see report for details

      if (value.find("Edge") > 0) {
        browser.set("MS Edge"); 
      } 
      else if (value.find("Opera") > 0 || value.find("OPR") > 0) {
        browser.set("Opera"); 
      }
      else if (value.find("Chrome") > 0) {
        browser.set("Google Chrome"); 
      }
      else if (value.find("Safari") > 0) {
        browser.set("Apple Safari"); 
      }
      else if (value.find("Firefox") > 0) {
        browser.set("Mozilla Firefox"); 
      } 
      else if (value.find("MSIE") > 0 || value.find("rv:11.0") > 0) {
        browser.set("MS IE"); 
      }
      else {
        browser.set("Other or undefined"); 
      }

    	context.write(browser, one); 
    }
  }

  // Reducer<KEYIN,VALUEIN,KEYOUT,VALUEOUT>
  public static class CustomReducer
       extends Reducer<Text,IntWritable,Text,IntWritable> {

    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {

      // there is an array of values for every unique key
      // summarizing this array for each key is done on this step

      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }

      context.write(key, new IntWritable(sum));
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();

    Job job = Job.getInstance(conf, "Browser count"); 
    job.setJarByClass(HW1.class);               // find jar by one of the containing class
    job.setMapperClass(CustomMapper.class);     
    job.setReducerClass(CustomReducer.class);   

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);

    job.setOutputFormatClass(SequenceFileOutputFormat.class); //snappy below

    FileInputFormat.addInputPath(job, new Path(args[0]));
    // FileOutputFormat.setOutputPath(job, new Path(args[1]));  // for txt out
    SequenceFileOutputFormat.setOutputPath(job, new Path(args[1]));

    FileOutputFormat.setCompressOutput(job,true);
    FileOutputFormat.setOutputCompressorClass(job,SnappyCodec.class);
    SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

    System.exit(job.waitForCompletion(true) ? 0 : 1); 
  }
}