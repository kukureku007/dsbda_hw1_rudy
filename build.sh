#!/bin/bash
export HADOOP_CLASSPATH=$(hadoop classpath)
javac -classpath ${HADOOP_CLASSPATH} -d classes src/HW1.java
jar -cvf HW1.jar -C classes .
hadoop fs -mkdir /HW1_rudy
hadoop fs -mkdir /HW1_rudy/Input
echo "## BUILD DONE ##"